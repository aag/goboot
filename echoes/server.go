package echoes

import (
	"fmt"

	G "gopkg.in/gilmour-libs/gilmour-e-go.v5"
)

func echoReply(req *G.Request, resp *G.Message) {
	var msg string
	req.Data(&msg)
	fmt.Println("Echoserver: received", msg)
	resp.SetData(fmt.Sprintf("Pong %v", msg))
}

func bindListeners(g *G.Gilmour) {
	opts := G.NewHandlerOpts().SetGroup("exclusive")
	g.ReplyTo("echo", echoReply, opts)
}

// RunServer starts the echo server
func RunServer() *G.Gilmour {
	engine := echoEngine()
	bindListeners(engine)
	engine.Start()
	return engine
}
