package echoes

import (
	"gopkg.in/gilmour-libs/gilmour-e-go.v5/backends/redis"

	G "gopkg.in/gilmour-libs/gilmour-e-go.v5"
)

func echoEngine() *G.Gilmour {
	r := redis.MakeRedis("127.0.0.1:6379", "")
	engine := G.Get(r)
	return engine
}
