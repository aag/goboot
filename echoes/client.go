package echoes

import (
	"fmt"
	"sync"

	G "gopkg.in/gilmour-libs/gilmour-e-go.v5"
)

func echoRequest(wg *sync.WaitGroup, engine *G.Gilmour, msg string) {
	req := engine.NewRequest("echo")
	resp, err := req.Execute(G.NewMessage().SetData(msg))
	if err != nil {
		fmt.Println("Echoclient: error", err.Error())
	}

	defer wg.Done()

	var output string
	if err := resp.Next().GetData(&output); err != nil {
		fmt.Println("Echoclient: error", err.Error())
	} else {
		fmt.Println("Echoclient: received", output)
	}
}

// RunClient runs the client 10 times
func RunClient(engine *G.Gilmour) {
	var wg sync.WaitGroup
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go echoRequest(&wg, engine, fmt.Sprintf("Hello: %v", i))
	}

	wg.Wait()
}
