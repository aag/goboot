package concurrency

import "time"

func produce(c chan int) {
	for i := 1; ; i = i + 1 {
		c <- i
		time.Sleep(100 * time.Millisecond)
	}
}
func consume(c chan int) {
	for {
		println(<-c)
	}
}

func DemoProducerConsumer() {
	// simple producer and consumer to demonstrate Communication with Synchronization
	c := make(chan int)
	go produce(c)
	go consume(c)
	time.Sleep(time.Second)
}
