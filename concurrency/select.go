package concurrency

import (
	"fmt"
	"math/rand"
	"time"
)

func viewProduct() <-chan string {
	views := make(chan string)
	go func() {
		for {
			views <- fmt.Sprintf("view product %d", rand.Intn(5))
			time.Sleep(time.Duration(rand.Intn(100)) * time.Millisecond)
		}
	}()
	return views
}

func buyProduct() <-chan string {
	purchases := make(chan string)
	go func() {
		for {
			purchases <- fmt.Sprintf("buy product %d", rand.Intn(5))
			time.Sleep(time.Duration(rand.Intn(300)) * time.Millisecond)
		}
	}()
	return purchases
}

func DemoSelect() {
	views := viewProduct()
	purchases := buyProduct()
	for i := 0; i < 10; i = i + 1 {
		select {
		case v := <-views:
			fmt.Println(v)
		case p := <-purchases:
			fmt.Println(p)
		default:
			fmt.Println("no activity")
		}
		time.Sleep(50 * time.Millisecond)
	}
}
