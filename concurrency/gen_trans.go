package concurrency

import (
	"fmt"
	"time"
)

func factorial(x int) int {
	f := 1
	for i := 1; i <= x; i = i + 1 {
		f = f * i
	}
	return f
}

func generate() <-chan int {
	c := make(chan int)
	go func() {
		for i := 1; ; i++ {
			c <- i
			time.Sleep(100 * time.Millisecond)
		}
	}()
	return c
}

func transform(src <-chan int, f func(int) int) <-chan int {
	dest := make(chan int)
	go func() {
		for {
			dest <- f(<-src)
		}
	}()
	return dest
}

func DemoStreamingDataTransformer() {
	ints := generate()
	facts := transform(ints, factorial)
	for i := 1; i < 10; i = i + 1 {
		fmt.Printf("%d factorial is %d\n", i, <-facts)
	}
}
