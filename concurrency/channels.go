package concurrency

import "fmt"

func printInt(c <-chan int) {
	fmt.Println(<-c)
}

func sendInt(c chan<- int) {
	c <- 1
}

func DemoChannels() {
	//c := make(chan int)
	//c <- 1
	//fmt.Println(<-c)

	//	c := make(chan int)
	//	go sendInt(c)
	//	fmt.Println(<-c)

	c := make(chan int)
	go printInt(c)
	c <- 1
}
