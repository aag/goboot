package concurrency

import "fmt"

func write(msg string) {
	fmt.Println(msg)
}

func syncWrite(msg string, done chan<- struct{}) {
	write(msg)
	done <- struct{}{}
}

func DemoGoroutines() {
	// Go program exits when main completes
	go write("Hello")
	// add a sleep in main so that goroutine executes
	// time.Sleep(time.Second)

	//done := make(chan struct{})
	//go syncWrite("Hello", done)
	//<-done
}
