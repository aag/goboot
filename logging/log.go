package logging

import (
	// example of local package alias
	log "github.com/Sirupsen/logrus"
	"github.com/johntdyer/slackrus"
)

// TextLog prints a sample log in text format
func TextLog() {
	log.SetFormatter(&log.TextFormatter{}) // default
	log.WithFields(log.Fields{
		"org":    "oogway",
		"person": "aditya",
	}).Info("Hello, World")
}

// JSONLog prints a sample log in json format
func JSONLog() {
	log.SetFormatter(&log.JSONFormatter{}) // default
	log.WithFields(log.Fields{
		"org":    "oogway",
		"person": "aditya",
	}).Info("Hello, World")
}

// SlackJSONLog prints a sample log in json format to a slack channel
func SlackJSONLog() {
	log.AddHook(&slackrus.SlackrusHook{
		HookURL:        "https://hooks.slack.com/services/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",
		AcceptedLevels: slackrus.LevelThreshold(log.WarnLevel),
		//		Channel:        "#slack-testing",
		//		IconEmoji:      ":ghost:",
		Username: "goboot logbot",
	})
	fieldLogger := log.WithFields(log.Fields{
		"org":    "oogway",
		"person": "aditya",
	})
	fieldLogger.Info("Hello, World")
	fieldLogger.Warn("Hello, World")
}
