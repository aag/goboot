package design

import "fmt"

type parent struct {
	name string
}

func (p *parent) GetName() string { return p.name }

type child struct {
	*parent
	surname string
}

func (d *child) Speak() {
	fmt.Println("My name is " + d.GetName() + " " + d.surname)
}

func makeChild(name, surname string) *child {
	return &child{
		parent:  &parent{name: name},
		surname: surname,
	}
}

// Inherit demonstrates simple inheritance pattern
func Inherit() {
	d := makeDerived("Master", "Oogway")
	d.Speak()
}
