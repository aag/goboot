package design

import "fmt"

type fullnamed interface {
	GetFullName(string) string
}

type person struct {
	fullnamed
	name string
}

func (n *person) Speak() {
	fmt.Println("My name is " + n.GetFullName(n.name))
}

type nameExt struct {
	surname string
}

func (e *nameExt) GetFullName(name string) string {
	return name + " " + e.surname
}

// PolicyMech demonstrates policy-mechinisn design
func PolicyMech() {
	f := &person{
		name:      "Master",
		fullnamed: &nameExt{surname: "Oogway"},
	}
	f.Speak()
}
