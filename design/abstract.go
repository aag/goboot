package design

import "fmt"

////////// Abstract parent class //////////
type nameMaker interface {
	MakeName() string
}

type abstract struct {
	nameMaker
	name string
}

func (a *abstract) GetName() string {
	return a.name
}

func (a *abstract) Speak() {
	fmt.Println("My name is " + a.MakeName())
}

///////////// Derived class ///////////////////

type derived struct {
	*abstract
	surname string
}

func (d *derived) MakeName() string {
	return d.GetName() + " " + d.surname
}

func makeDerived(name, surname string) *derived {
	parent := &abstract{name: name}
	child := &derived{abstract: parent, surname: surname}
	parent.nameMaker = child
	return child
}

// AbstractInh is a function to demostrate abstract inheritence
func AbstractInh() {
	d := makeDerived("Master", "Oogway")
	d.Speak()
}
