package bp

import (
	"log"
	"sync"
)

type SingletonStruct struct {
	n int
}

var global SingletonStruct
var once sync.Once

func Init(n int) {
	once.Do(func() {
		log.Println("Initialising package bp")
		global = SingletonStruct{n: n}
	})
}

func GetVal() int { return global.n }
