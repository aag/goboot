package bp

import "fmt"

func NamedRet() {
	fmt.Printf("fact = %d\n", fact(10))
	val, err := erroringFact(10)
	if err != nil {
		fmt.Printf("val = %+v\n", val)
	}
}

func fact(n int) (ret int) {
	ret = 1
	for n > 1 {
		ret *= n
		n--
	}
	return
}

func erroringFact(n int) (ret int, err error) {
	if err = makeError(); err != nil { // shadowing is a compiler error
		return
	}
	ret = fact(n)
	return
}

func makeError() error {
	//	return fmt.Errorf("error")
	return nil
}
