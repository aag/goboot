package bp

import "fmt"

type escaped struct {
	a int
	b int
}

func getStackVar() escaped {
	e := escaped{
		a: 3, b: 4,
	}
	fmt.Printf("getStackVar address = %p\n", &e)
	return e
}

func getStackVarP() *escaped {
	e := escaped{
		a: 3, b: 4,
	}
	fmt.Printf("getStackVarP address = %p\n", &e)
	return &e
}

// Esc demostrates escape analysis
func Esc() {
	stackVarP := getStackVarP()
	fmt.Printf("getStackVarP in Esc address = %p\n", stackVarP)
	stackVar := getStackVar()
	fmt.Printf("getStackVar in Esc address = %p\n", &stackVar)
}
