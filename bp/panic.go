package bp

import "fmt"

func top() {
	defer first()
	defer second()
	//	panicker() // u1
}

func panicker() {
	panic("Help")
}

func first() {
	//	if r := recover(); r != nil { // u4
	//		fmt.Println("Don't panic")
	//	}
	fmt.Println("I'm first")
}

func second() {
	//	if r := recover(); r != nil { // u2
	//		fmt.Println("Don't panic")
	//		// panic("No, I will still panic") // u3
	//	}
	fmt.Println("I'm second")
}

// Panic demonstrates panic and defers
func Panic() {
	top()
}
