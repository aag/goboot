package bp

import "fmt"

type bank func(n int) int

func makeBank(n int) bank {
	balance := n
	return func(diff int) int {
		balance += diff
		return balance
	}
}

// Closure demonstrates closures and first class functions
func Closure() {
	bank := makeBank(10)
	fmt.Println(bank(10))
	fmt.Println(bank(-5))
	fmt.Println(bank(5))
}
