package bp

import "fmt"

func makeBanks(initials []int) []bank {
	banks := []bank{}
	for _, initial := range initials {
		initial := initial
		bank := func(diff int) int {
			initial += diff
			return initial
		}
		banks = append(banks, bank)
	}
	return banks
}

// LoopClosure demonstrates gotcha with loop variables and closures
func LoopClosure() {
	initial := []int{10, 20}
	banks := makeBanks(initial)
	bank1 := banks[0]
	bank2 := banks[1]
	fmt.Println(bank1(10))
	fmt.Println(bank2(10))
}
