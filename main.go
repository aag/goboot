package main

import (
	"fmt"

	"bitbucket.org/aag/goboot/concurrency"
	"bitbucket.org/aag/goboot/design"
	"bitbucket.org/aag/goboot/echoes"
	"bitbucket.org/aag/goboot/err"
	"bitbucket.org/aag/goboot/logging"
	"bitbucket.org/aag/goboot/reflection"
	"bitbucket.org/aag/goboot/structure"
)

func FormatJSON() {
	// this is just a method to demonstrate govet and golint
	return
	return
}

func main() {
	// demoImports()
	// demoEcho()
	// demoLog()
	// demoErr()
	// demoBP()
	// demoDesign()
	demoConc()
	// demoReflection()
}

func demoImports() {
	structure.Imports()
}

func demoEcho() {
	engine := echoes.RunServer()
	echoes.RunClient(engine)
}

func demoLog() {
	logging.TextLog()
	logging.JSONLog()
	logging.SlackJSONLog()
}

func demoErr() {
	err0 := err.RegularError()
	fmt.Printf("%+v\n", err0)
	err1 := err.WrappedError()
	fmt.Printf("%+v\n", err1)
	// err.RegularError() // caught by metalinter
}

func demoBP() {
	// bp.NamedRet()
	//	{ // Singleton
	//		bp.Init(10)
	//		bpval := bp.GetVal()
	//		fmt.Printf("bpval = %+v\n", bpval)
	//		bp.Init(20)
	//		bpval = bp.GetVal()
	//		fmt.Printf("bpval = %+v\n", bpval)
	//	}
	//  bp.Closure()
	//  bp.LoopClosure()
	//  bp.Panic()
	//  bp.Esc()
	//	ReadCloser. ioutils.ReadCopy
}

func demoDesign() {
	design.Inherit()
	//	design.AbstractInh()
	//	design.PolicyMech()
}

func demoReflection() {
	reflection.Reflected()
	//	reflection.ReflectedSum()
}

func demoConc() {
	//	concurrency.DemoGoroutines()
	//	concurrency.DemoChannels()
	// concurrency.DemoProducerConsumer()
	// concurrency.DemoStreamingDataTransformer()
	concurrency.DemoSelect()
}
