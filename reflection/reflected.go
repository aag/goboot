package reflection

import "fmt"

// Reflected is demo code using the definitions in reflect_cons.go
// Notice the type conversions
// Also the list is not type safe. One can even do
// `lst = lst.Cons(Cons("imastring"))` and the compiler
// wont complain and you will get a runtime panic
func Reflected() {
	lst := Cons(1)
	lst = lst.Cons(2)
	first := lst.Car().(int)
	second := lst.Cdr().Car().(int)
	fmt.Println(first)
	fmt.Println(second)
	fmt.Printf("Add: %d\n", first+second)
}

// Integer wraps int to add methods
type Integer int

// Add adds two integers
// Have to create a wrapper type with a Add method
// since reflect package cannot be used to call
// operators like "+"
func (i Integer) Add(j Integer) Integer {
	return Integer(i + j)
}

// ReflectedSum is a generic sum using reflection
func ReflectedSum() {
	lst := Cons(Integer(1))
	lst = lst.Cons(Integer(2))
	fmt.Println(lst.Sum().(Integer) + 1)
	if val, ok := lst.Sum().(Integer); ok {
		fmt.Println(val + 1)
	}
}
