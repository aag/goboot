package reflection

// This code is an implementation of a Cons pair and a "generic" Sum
// method using reflection
import "reflect"

// ConsP is a cons cell
type ConsP struct {
	car interface{}
	cdr *ConsP
}

// Cons wraps a value in a cons cell
func Cons(t interface{}) *ConsP {
	return &ConsP{
		car: t,
	}
}

// Car returns the car of the cons
func (p *ConsP) Car() interface{} {
	return p.car
}

// Cdr returns the cdr of the cons
func (p *ConsP) Cdr() *ConsP {
	return p.cdr
}

// Cons conses a new value
func (p *ConsP) Cons(v interface{}) *ConsP {
	c := Cons(v)
	c.cdr = p
	return c
}

// Sum of list using recursive sum algorithm
// lst.sum = lst.car + lst.cdr.sum
func (p *ConsP) Sum() interface{} {
	//	return "hello"
	v := reflect.ValueOf(p.car) // Get Value from interface{}
	t := reflect.TypeOf(p.car)  // Get Type from interface
	// Exit condition
	if p.cdr == nil {
		return v.Interface()
	}
	// Fetch the method to call
	m, _ := t.MethodByName("Add")
	// Setup method args
	args := make([]reflect.Value, 2)
	args[0] = v
	args[1] = reflect.ValueOf(p.cdr.Sum())
	val := m.Func.Call(args)[0] // Call
	return val.Interface()      // Wrap in interface and return
}
