package err

import (
	"fmt"
	"log"

	"github.com/pkg/errors"
)

// RegularError just returns a regular error
func RegularError() error {
	err := innerError()
	log.Println("Top level error message")
	return err

}

func innerError() error {
	err := innerError1()
	log.Println("Error at level 1")
	return err
}

// WrappedError returns wrapper error
func WrappedError() error {
	return errors.Wrap(innerError0(), "Top level error message")
}

func innerError0() error {
	return errors.Wrapf(innerError1(), "Error at level %d", 1)
}

func innerError1() error {
	return fmt.Errorf("Original error")
}
