package structure

import (
	"bitbucket.org/aag/goboot/structure/sub"
)

// Imports uses an imported function
func Imports() {
	sub.ImportedFunction()
}
